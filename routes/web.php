<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('backoffice.home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::middleware('auth')->group(function () {
    Route::resource('products','ProductsController')->except([
        "show"
    ]);
    Route::resource('categories','CategoriesController')->except([
        "show"
    ]);
    /*
		Commands Controller
    */
	Route::get('commands','CommandsController@index')->name('commands.list');
	Route::get('commands/{command}','CommandsController@consulte')->name('commands.consulte');
	Route::delete('commands/delete/{command}','CommandsController@remove')->name('commands.remove');
});
