<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::namespace('API')->group(function () {
    /*
        User
    */
    Route::post('login', 'UserController@login');
    Route::post('register', 'UserController@register');
    Route::middleware('auth:api', 'throttle:60,1')->group(function () {
        Route::get('me','UserController@me');
        Route::patch('update','UserController@update');
        Route::patch('edit_password','UserController@edit_password');
        /*
            Cart and Command
        */
        Route::get('commands','CommandController@commands');
        Route::get('cart','CommandController@cart');
        Route::post('add_to_cart','CommandController@add_to_cart');
        Route::delete('remove_from_cart','CommandController@remove_from_cart');
        Route::delete('empty_cart','CommandController@empty_cart');
        Route::post('command','CommandController@create_command');
        Route::delete('command/cancel/{command}','CommandController@cancel_command');
        Route::delete('command/delete/{command}','CommandController@delete_command');
    });
    /*
        Products and Categories
    */
    Route::resource('categories','CategoriesController')->only([
        'index','show'
    ]);
    Route::resource('products','ProductsController')->only([
        'show'
    ]);
});
