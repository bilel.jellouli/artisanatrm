<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Category::class, 5)
            ->create()
            ->each(function ($category) {
                $category->products()->createMany(factory(App\Product::class,10)->make()->toArray());
            });
    }
}
