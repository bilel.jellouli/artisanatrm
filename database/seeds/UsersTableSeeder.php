<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class)->make([
            "name" => "admin",
            "email" => "admin@artisanatrm.com",
        ])->save();
        factory(App\User::class,10)->create();

    }
}
