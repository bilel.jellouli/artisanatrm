<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    $images = array_fill(0, 3, $faker->imageUrl(640,480));
    return [
        "title" => $faker->word,
        "description" => $faker->paragraph,
        "price" => $faker->randomFloat(null,1,999),
        "main_image" => $faker->imageUrl(640,480),
        "images" => implode(",", $images),
        "stock" => $faker->numberBetween(0,100)
    ];
});
