<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Command extends Model
{

	use SoftDeletes;

    protected $guarded = [];

    protected $hidden = ["content"];

    public function user ()
    {
        return $this->belongsTo(User::class);
    }

    public function getTotalAttribute ($content)
    {
    	return json_decode($this->content)->total;
    }
}
