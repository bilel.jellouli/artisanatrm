<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("backoffice.categories.index",[
            "categories" => Category::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backoffice.categories.form',[
            "category" => new Category
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            "title" => "required"
        ]);
        $fill = $request->only('title');
        if ($request->has('description')) {
            $fill['description'] = $request->description;
        }
        if ($request->has('cover')) {
            $filename = Str::slug($request->title).".".$request->cover->getClientOriginalExtension();
            $fill['cover'] = asset('storage/uploads/'.$filename);
            $request->cover->storeAs('uploads',$filename,'public');
        }
        Category::create($fill);
        return redirect()->route('categories.index')->with("alert",[
            "type" => "success",
            "message" => "Category has been added"
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('backoffice.categories.form',[
            "category" => $category
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $this->validate($request,[
            "title" => "required"
        ]);
        $fill = $request->only('title','description');
        if ($request->has('cover')) {
            $old_image = last(explode('/', $category->cover));
            @unlink(storage_path('app/public/uploads/'.$old_image));
            $filename = Str::slug($request->title).".".$request->cover->getClientOriginalExtension();
            $fill['cover'] = asset('storage/uploads/'.$filename);
            $request->cover->storeAs('uploads',$filename,'public');
        }
        $category->update($fill);
        return redirect()->route('categories.index')->with("alert",[
            "type" => "success",
            "message" => "Category has been updated"
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        if ($category->products->count() > 0) {
            return redirect()->back()->with("alert",[
                "type" => "danger",
                "message" => "Category can not be deleted because it have products on it"
            ]);
        }
        $category->delete();
        return redirect()->route('categories.index')->with("alert",[
            "type" => "info",
            "message" => "Category has been deleted"
        ]);
    }
}
