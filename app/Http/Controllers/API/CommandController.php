<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Cache;
use App\{
	Product,
	Command
};

class CommandController extends Controller
{
    public function commands (Request $request) {
        return response()->json([
            "success" => true,
            "commands" => $request->user()->commands()->orderBy('created_at','DESC')->get()
        ]);
    }

    public function cart (Request $request) {
        if (!Cache::has("cart_".$request->user()->id)) {
            return response()->json([
                "cart" => [
                    "products" => [],
                    "total" => 0
                ]
            ]);
        }
        return response()->json([
            "cart" => Cache::get("cart_".$request->user()->id)
        ]);
    }

    public function add_to_cart (Request $request) {
        $product = Product::findOrFail($request->product);
        $ammount = $request->has("ammount") ? $request->ammount : 1;
        if ($ammount > $product->stock) {
            return response()->json([
                "success" => false,
                "message" => "We don't have that qunatity in our stock max(".$product->stock.")"
            ]);
        }
        $cart = (Cache::has('cart_'.$request->user()->id)) ? Cache::get('cart_'.$request->user()->id) : [
            "products" => [],
            "total" => 0
        ];
        $ammount = (isset($cart["products"][$product->id])) ? ($cart["products"][$product->id] + $ammount) : $ammount;
        if ($ammount > $product->stock) {
            return response()->json([
                "success" => false,
                "message" => "We don't have that qunatity in our stock max(".$product->stock.")"
            ]);
        }
        if ($cart["total"] != 0 && isset($cart["products"][$product->id])) {
            $cart["total"] -= $cart["products"][$product->id] * $product->price;
        }
        $cart["products"][$product->id] = $ammount;
        $cart["total"] += $cart["products"][$product->id] * $product->price;
        Cache::put('cart_'.$request->user()->id, $cart, now()->addHours(4));
        return response()->json([
            "success" => true,
            "message" => "Product has been add to the cart",
            "cart" => $cart
        ]);
    }

    public function remove_from_cart (Request $request) {
        if (!Cache::has('cart_'.$request->user()->id)) {
            return response()->json([
                "success" => false,
                "message" => "Your cart is still empty"
            ]);
        }
        $cart = Cache::get("cart_".$request->user()->id);
        if (count($cart["products"]) == 0 || !isset($cart["products"][$request->product])) {
            return response()->json([
                "success" => false,
                "message" => "this object doesn't exist in your cart"
            ]);
        }
        $product = Product::findOrFail($request->product);
        $cart["total"] -= $cart["products"][$request->product] * $product->price;
        unset($cart["products"][$request->product]);
        Cache::put('cart_'.$request->user()->id, $cart, now()->addHours(4));
        return response()->json([
            "success" => true,
            "message" => "Object was removed from your cart",
            "cart" => $cart
        ]);
    }

    public function empty_cart (Request $request) {
        if (!Cache::has("cart_".$request->user()->id)) {
            return response()->json([
                "success" => false,
                "message" => "Your cart is already empty"
            ]);
        }
        Cache::forget("cart_".$request->user()->id);
        return response()->json([
            "success" => true,
            "message" => "Your cart is reseted",
            "cart" => [
                "products" => [],
                "total" => 0
            ]
        ]);
    }

    public function create_command (Request $request) {
    	$this->validate($request,[
    		"adresse" => "required"
    	]);
        if (!Cache::has("cart_".$request->user()->id)) {
            return response()->json([
                "success" => false,
                "message" => "Your cart is still empty"
            ]);
        }
        $cart = Cache::get("cart_".$request->user()->id);
        if (count($cart["products"]) == 0) {
            return response()->json([
                "success" => false,
                "message" => "Your cart is still empty"
            ]);
        }
        $cart["delevery_adresse"] = $request->adresse;
        $command = $request->user()->commands()->create([
            "content" => json_encode($cart),
            "status" => 0
        ]);
        Cache::forget("cart_".$request->user()->id);
        return response()->json([
            "success" => true,
            "message" => "Command created successfully",
            "command" => $command
        ]);
    }

    public function cancel_command(Request $request,Command $command)
    {
    	if ($command->user_id != $request->user()->id) {
    		return response()->json([
    			"success" => false,
    			"You can not delete a command that doesn't belongs to you"
    		]);
    	}
    	if (in_array($command->status, ["2","3","99"])) {
    		return response()->json([
    			"success" => false,
    			"message" => "Command can not be canceled please contact the admins"
    		]);
    	}
    	if ($command->status == "98") {
    		return response()->json([
    			"success" => false,
    			"message" => "Command is already cancelled"
    		]);
    	}
    	$command->update([
    		"status" => 98
    	]);
    	return response()->json([
			"success" => true,
			"message" => "Command is cancelled"
		]);
    }

    public function delete_command(Request $request,Command $command)
    {
    	if ($command->user_id != $request->user()->id) {
    		return response()->json([
    			"success" => false,
    			"You can not delete a command that doesn't belongs to you"
    		]);
    	}
    	if (in_array($command->status, ["1","2","99"])) {
    		return response()->json([
    			"success" => false,
    			"message" => "Command can not be deleted at this stage please contact the admins"
    		]);
    	}
    	if ($command->status != "3") {
	    	$command->update([
	    		"status" => 98
	    	]);
    	}
    	$command->delete();
    	return response()->json([
			"success" => true,
			"message" => "Command is cancelled"
		]);
    }
}
