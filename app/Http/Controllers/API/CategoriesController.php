<?php

namespace App\Http\Controllers\API;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index ()
    {
        return response()->json([
            "success" => true,
            "categories" => Category::all()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show (Category $category)
    {
        return response()->json([
            "success" => true,
            "category" => $category,
            "products" => $category->products()->paginate(3)
        ]);
    }
}
