<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function register (Request $request)
    {
        $email_validation = User::where('email',$request->email)->count();
        if ($email_validation != 0) {
            return response()->json([
                "success" => false,
                "message" => "This email already registered please use another email or try to login"
            ]);
        }
        $user = new User();
        $token = $user->generate_token();
        $user->fill([
            "name" => $request->name,
            "email" => $request->email,
            "password" => bcrypt($request->password),
            "api_token" => $token,
        ])->save();
        return response()->json([
            "success" => true,
            "user" => $user
        ]);
    }

    public function login (Request $request)
    {
        $credentials = $request->only('email', 'password');
        if (!Auth::attempt($credentials)) {
            return response()->json([
                "success" => false,
                "message" => "Login fail your email or password are incorrect"
            ]);
        } else {
            $token = Auth::user()->generate_token();
            Auth::user()->fill([
                "api_token" => $token
            ])->save();
            return response()->json([
                "success" => true,
                "user" => Auth::user()
            ]);
        }
    }

    public function me (Request $request)
    {
        return response()->json([
            "success" => true,
            "user" => $request->user()
        ]);
    }

    public function update (Request $request)
    {
        $user = $request->user();
        $user->update($request->only([
            "email","name"
        ]));
        return response()->json([
            "success" => true,
            "user" => $user
        ]);
    }

    public function edit_password (Request $request)
    {
        $credentials = [
            "email" => $request->user()->email,
            "password" => $request->only('current_password')["current_password"]
        ];
        if (!Auth::attempt($credentials)) {
            return response()->json([
                "success" => false,
                "message" => "incorrect password"
            ]);
        } else {
            $token = Auth::user()->generate_token();
            $request->user()->fill([
                'api_token' => $token,
                'password' => $request->new_password
            ]);
            return response()->json([
                "success" => true,
                "message" => "Your password has been updated"
            ]);
        }
    }
}
