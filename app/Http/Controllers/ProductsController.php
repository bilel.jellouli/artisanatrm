<?php

namespace App\Http\Controllers;

use App\{
    Product,
    Category
};
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backoffice.products.index',[
            'products' => Product::with('category')->orderBy('id','DESC')->paginate(15)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backoffice.products.form',[
            "product" => new Product,
            "categories" => Category::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            "title" => "required",
            "description" => "required",
            "price" => "required",
            "main_image" => "required|image",
            "category_id" => "required",
            "stock" => "required",
        ]);
        $fill = $request->only('title','description','price','category_id','stock');
        $fill['price'] = (float)$fill['price'];
        $filename = Str::slug($request->title).".".$request->main_image->getClientOriginalExtension();
        $fill['main_image'] = asset('storage/uploads/'.$filename);
        $request->main_image->storeAs('uploads',$filename,'public');
        Product::create($fill);
        return redirect()->route('products.index')->with('alert',[
            "type" => "success",
            "message" => "Product has been added to database"
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view("backoffice.products.form",[
            "product" => $product,
            "categories" => Category::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $fill = $this->validate($request,[
            "title" => "required",
            "description" => "required",
            "price" => "required",
            "category_id" => "required",
            "stock" => "required",
        ]);
        if ($request->has('main_image')) {
            $old_image = last(explode('/', $product->main_image));
            unlink(storage_path('app/public/uploads/'.$old_image));
            $filename = Str::slug($request->title).".".$request->main_image->getClientOriginalExtension();
            $fill['main_image'] = asset('storage/uploads/'.$filename);
            $request->main_image->storeAs('uploads',$filename,'public');
        }
        $product->update($fill);
        return redirect()->route('products.index')->with('alert',[
            "type" => "success",
            "message" => "Product has been added to database"
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->route('products.index')->with('alert',[
            "type" => "info",
            "message" => "Product has been deleted"
        ]);
    }
}
