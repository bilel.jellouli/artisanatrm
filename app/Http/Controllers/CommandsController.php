<?php

namespace App\Http\Controllers;
use App\{
	Command,
	Product
};

use Illuminate\Http\Request;

class CommandsController extends Controller
{
    public function index ()
    {
    	$commands = Command::withTrashed()->with('user')->get()->groupBy('status');
    	if (!isset($commands["0"])) {
    		$commands["0"] = collect([]);
    	}
    	if (!isset($commands["1"])) {
    		$commands["1"] = collect([]);
    	}
    	if (!isset($commands["2"])) {
    		$commands["2"] = collect([]);
    	}
    	if (!isset($commands["3"])) {
    		$commands["3"] = collect([]);
    	}
    	if (!isset($commands["98"])) {
    		$commands["98"] = collect([]);
    	}
    	if (!isset($commands["99"])) {
    		$commands["99"] = collect([]);
    	}
    	return view('backoffice.commands.index',[
    		"commands_grouped" => $commands
    	]);
    }

    public function consulte ($command) {
    	$command = Command::with('user')->withTrashed()->findOrFail($command);
    	$command_content = json_decode($command->content,true);
    	$command_products = Product::with('category')->whereIn('id',array_keys($command_content["products"]))->get()->keyBy('id');
    	$index = 0;
    	$lines = [];
    	foreach ($command_content["products"] as $product_id => $quntity) {
    		$index++;
    		$lines[] = [
    			$index,
    			$command_products[$product_id]->category->title,
    			$command_products[$product_id]->title,
    			number_format($command_products[$product_id]->price,3),
    			$quntity,
    			number_format($command_products[$product_id]->price * (int)$quntity , 3)
    		];
    	}
    	$command->lines = $lines;
    	$command->delevery_adresse = $command_content["delevery_adresse"];
    	return view("backoffice.commands.consulte",[
    		"command" => $command
    	]);
    }

    public function remove (Command $command) {
    	$command->forceDelete();
    	return redirect()->back()->with('alert',[
    		"type" => "danger",
    		"message" => "Command has been removed"
    	]);
    }
}
