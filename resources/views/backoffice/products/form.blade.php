@extends('layouts.backoffice')

@section('content')
  <!-- .row -->
  <div class="row">
      <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
                <h5 class="card-title block">{{ (is_null($product->id)) ? 'Add product' : 'Edit: '.$product->title }} </h5>
                <br>
                @if(session('alert'))
                    <div class="my-4">
                        <div class="alert alert-{{ session('alert')['type'] }}">
                            {{ session('alert')['message'] }}
                        </div>
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="my-4">
                    <form method="post" action="{{ (is_null($product->id)) ? route('products.store') : route('products.update',$product->id) }}" enctype="multipart/form-data">
                        @csrf
                        @if(!is_null($product->id))
                            @method('PUT')
                        @endif
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" name="title" id="title" placeholder="Product title" value="{{ old('title',$product->title) }}">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" name="description" id="description" placeholder="Product description">{{ old('description',$product->description) }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="price">Price</label>
                            <input type="text" class="form-control" name="price" id="price" placeholder="Product price" value="{{ old('price',$product->price) }}">
                        </div>
                        <div class="form-group">
                            <label for="category">Category</label>
                            <select type="text" class="form-control" name="category_id" id="category">
                                <option value="" {{ (is_null($product->category_id)) ? 'selected' : ''  }}>Select a category</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}" {{ ($category->id == $product->category_id || $category->id == old('category_id')) ? 'selected' : '' }}>{{ $category->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="stock">Stock</label>
                            <input min="0" type="number" class="form-control" name="stock" id="stock" placeholder="Product stock" value="{{ old('stock',$product->stock) }}">
                        </div>
                        <div class="form-group">
                            <label for="main_image">Main image</label>
                            <input min="0" type="file" class="form-control-file" name="main_image" id="main_image">
                        </div>
                        <button type="submit" class="btn btn-success">{{ (is_null($product->id)) ? 'Save' : 'Update'}}</button>
                    </form>
                </div>
            </div>
          </div>
      </div>
  </div>
  <!-- /.row -->
@endsection
