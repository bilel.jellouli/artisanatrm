@extends('layouts.backoffice')

@section('content')
  <!-- .row -->
  <div class="row">
      <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
                <h5 class="card-title">Products</h5>
                @if(session('alert'))
                    <div class="my-4">
                        <div class="alert alert-{{ session('alert')['type'] }}">
                            {{ session('alert')['message'] }}
                        </div>
                    </div>
                @endif
                @if($products->count() > 0)
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Title</th>
                            <th>Category</th>
                            <th>Created at</th>
                            <th>Last update</th>
                            <th width="200px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($products as $product)
                            <tr>
                                <td>{{ $product->id }}</td>
                                <td>{{ $product->title }}</td>
                                <td>{{ $product->category->title }}</td>
                                <td>{{ $product->created_at->format('d/m/Y H:i:s') }}</td>
                                <td>{{ $product->updated_at->format('d/m/Y H:i:s') }}</td>
                                <td>
                                    <a href="{{ route('products.edit',$product->id)}}" class="btn btn-sm btn-warning">edit</a>
                                    <form method="post" action="{{ route('products.destroy',$product->id) }}">
                                        @method('DELETE')
                                        @csrf
                                        <button class="btn btn-sm btn-danger">delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="my-4">
                    {{ $products->links() }}
                </div>
                @else
                    <div class="alert alert-info">
                        <h4>You have no products to display please <a href="{{ route('products.create') }}">create one</a> to get started</h4>
                    </div>
                @endif
            </div>
          </div>
      </div>
  </div>
  <!-- /.row -->
@endsection
