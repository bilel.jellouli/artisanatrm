@extends('layouts.backoffice')

@section('content')
  <!-- .row -->
  <div class="row">
	  <div class="col-lg-12">
		  <div class="card">
			<div class="card-body">
				<h5 class="card-title block">Clients commands</h5>
				<br>
				@if(session('alert'))
					<div class="my-4">
						<div class="alert alert-{{ session('alert')['type'] }}">
							{{ session('alert')['message'] }}
						</div>
					</div>
				@endif
				@if ($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				<div class="my-4">
					@foreach($commands_grouped as $status => $commands)
						<h4>{{ config('commands.status.'.$status) }}</h4>
						@if(count($commands) > 0)
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Id</th>
									<th>Client name</th>
									<th>Total</th>
									<th>Created at</th>
									<th>Last edit</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach($commands as $command)
								<tr>
									<td>{{ $command->id }}</td>
									<td>{{ $command->user->name }}</td>
									<td>{{ number_format($command->total,3) }} DTN</td>
									<td>{{ $command->created_at->format('d/m/Y H:i:s') }}</td>
									<td>{{ $command->updated_at->format('d/m/Y H:i:s') }}</td>
									<td>
										<a href="{{ route('commands.consulte',$command->id) }}" class="btn btn-xs btn-primary">View</a>
										<form action="{{ route('commands.remove',$command->id) }}" method="post">
											@method('DELETE')
											@csrf
											<button type="submit" class="btn btn-xs btn-danger">force delete</button>
										</form>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						@else
							<div class="alert alert-info">No commands with this status are availble</div>
						@endif
					@endforeach
				</div>
			</div>
		  </div>
	  </div>
  </div>
  <!-- /.row -->
@endsection
