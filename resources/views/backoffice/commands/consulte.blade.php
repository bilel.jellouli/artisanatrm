@extends('layouts.backoffice')

@section('content')
  <!-- .row -->
  <div class="row">
	  <div class="col-lg-12">
		  <div class="card">
			<div class="card-body">
				<h5 class="card-title block">Clients commands [{{ config('commands.status.'.$command->status) }}]</h5>
				<br>
				@if(session('alert'))
					<div class="my-4">
						<div class="alert alert-{{ session('alert')['type'] }}">
							{{ session('alert')['message'] }}
						</div>
					</div>
				@endif
				@if ($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				<div class="my-4">
					<h5>User info</h5>
					<table class="table">
						<tr>
							<th>name</th>
							<td>{{ $command->user->name }}</td>
						</tr>
						<tr>
							<th>email</th>
							<td>{{ $command->user->email }}</td>
						</tr>
						<tr>
							<th>Delevery adresse</th>
							<td>{{ $command->delevery_adresse }}</td>
						</tr>
					</table>
					<h5>Command details</h5>
					<table class="table table-border">
						<thead>
							<tr>
								<th>#</th>
								<th>Category</th>
								<th>Product</th>
								<th>Unit price</th>
								<th>Qunatity</th>
								<th>Total</th>
							</tr>
						</thead>
						<tbody>
							@foreach($command->lines as $line)
								<tr>
									@foreach($line as $column)
										<td>{{ $column }}</td>
									@endforeach
								</tr>
							@endforeach
						</tbody>
					</table>
					<h5>Total : {{ $command->total }} DTN</h5>
				</div>
			</div>
		  </div>
	  </div>
  </div>
  <!-- /.row -->
@endsection
