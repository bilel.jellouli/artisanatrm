@extends('layouts.backoffice')

@section('content')
  <!-- .row -->
  <div class="row">
      <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
                <h5 class="card-title block">{{ (is_null($category->id)) ? 'Add Category' : 'Edit: '.$category->title }} </h5>
                <br>
                @if(session('alert'))
                    <div class="my-4">
                        <div class="alert alert-{{ session('alert')['type'] }}">
                            {{ session('alert')['message'] }}
                        </div>
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="my-4">
                    <form method="post" action="{{ (is_null($category->id)) ? route('categories.store') : route('categories.update',$category->id) }}" enctype="multipart/form-data">
                        @csrf
                        @if(!is_null($category->id))
                            @method('PUT')
                        @endif
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" name="title" id="title" placeholder="Category title" value="{{ old('title',$category->title) }}">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" name="description" id="description" placeholder="Category description">{{ old('description',$category->description) }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="cover">Cover image</label>
                            <input min="0" type="file" class="form-control-file" name="cover" id="cover">
                        </div>
                        <button type="submit" class="btn btn-success">{{ (is_null($category->id)) ? 'Save' : 'Update'}}</button>
                    </form>
                </div>
            </div>
          </div>
      </div>
  </div>
  <!-- /.row -->
@endsection
