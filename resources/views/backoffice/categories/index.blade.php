@extends('layouts.backoffice')

@section('content')
  <!-- .row -->
  <div class="row">
      <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
                <h5 class="card-title">Categories</h5>
                @if(session('alert'))
                    <div class="my-4">
                        <div class="alert alert-{{ session('alert')['type'] }}">
                            {{ session('alert')['message'] }}
                        </div>
                    </div>
                @endif
                @if($categories->count() > 0)
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Title</th>
                            <th>Products</th>
                            <th>Created at</th>
                            <th>Last update</th>
                            <th width="200px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td>{{ $category->id }}</td>
                                <td>{{ $category->title }}</td>
                                <td>{{ $category->products->count() }}</td>
                                <td>{{ $category->created_at->format('d/m/Y H:i:s') }}</td>
                                <td>{{ $category->updated_at->format('d/m/Y H:i:s') }}</td>
                                <td>
                                    <a href="{{ route('categories.edit',$category->id)}}" class="btn btn-sm btn-warning">edit</a>
                                    <form method="post" action="{{ route('categories.destroy',$category->id) }}">
                                        @method('DELETE')
                                        @csrf
                                        <button class="btn btn-sm btn-danger">delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                    <div class="alert alert-info">
                        <h4>You have no categories to display please <a href="{{ route('categories.create') }}">create one</a> to get started</h4>
                    </div>
                @endif
            </div>
          </div>
      </div>
  </div>
  <!-- /.row -->
@endsection
